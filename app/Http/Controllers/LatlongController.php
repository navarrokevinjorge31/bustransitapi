<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\LatLong;
use Validator;
class LatlongController extends Controller
{
    public function create_latlong (Request $request){

    	 $validator = Validator::make($request->all(),[
			'lat' 	=> 'required',
			'long' => 'required',
            'route' => 'required',
            'mode' => 'required',
            'mode_info' => 'required',

		]);
		if ($validator->fails()) {
			return response()->json(['message' => implode(", ",$validator->messages()->all()), 
                                     'error' => true,
                                     'error_code' => 400,
                                     'line'    => "line ".__LINE__." ".basename(__FILE__)], 200);
		}

		$data = $request->all();
		$createbldg = LatLong::create($data);
        return response()->json(['message' => "Successfully Send data", 
                                 'error' => false,
                                 'error_code' => 200,
                                 'line'    => "line ".__LINE__." ".basename(__FILE__)], 200); 



    }


    public function getLatlong(Request $request){
       $test['latlong'] = LatLong::orderBy('id', 'desc')->first();
        return $test;
    }
}