<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\BusLocations;
use Validator;
class BusLocationController extends Controller
{
    public function create_bus_location (Request $request){

    	 $validator = Validator::make($request->all(),[
			'routeId' 	=> 'required',
			'longitude' => 'required',
            'latitude' => 'required',
		]);
		if ($validator->fails()) {
			return response()->json(['message' => implode(", ",$validator->messages()->all()), 
                                     'error' => true,
                                     'error_code' => 400,
                                     'line'    => "line ".__LINE__." ".basename(__FILE__)], 200);
		}
		$data = $request->all();
		$createLocation = BusLocations::create($data);
        return response()->json(['message' => "Successfully Send data", 
                                 'error' => false,
                                 'error_code' => 200,
                                 'line'    => "line ".__LINE__." ".basename(__FILE__)], 200); 
    }

    public function getBusLocations($id){
      $data['bus_locations'] = BusLocations::where('id', $id)->first();
        return $data;
    }
}