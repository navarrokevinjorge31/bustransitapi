<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusLocations extends Model
{

	 protected $table = 'bus_locations';
   
     protected $fillable = [
       		'routeId',
	  	  'longitude',
	  	  'latitude',
    ];
}