<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LatLong extends Model
{

	 protected $table = 'lat_long';
   
     protected $fillable = [
       		'lat',
	  	  'long',
	  	  'route',
	  	  'mode',
	  	  'mode_info',
    ];
}
