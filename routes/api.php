<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('latlong','LatlongController@create_latlong');
Route::get('get_latlong','LatlongController@getLatlong');

//creating static longlat
Route::post('bus_locations','BusLocationController@create_bus_location');
Route::get('get_bus_locations/{id}','BusLocationController@getBusLocations');