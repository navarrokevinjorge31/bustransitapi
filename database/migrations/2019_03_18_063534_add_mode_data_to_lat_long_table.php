<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddModeDataToLatLongTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lat_long', function (Blueprint $table) {
            $table->string('route')->after('id');
            $table->string('mode')->after('route');
            $table->string('mode_info')->after('mode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lat_long', function (Blueprint $table) {
            $table->dropColumn('route');
            $table->dropColumn('mode');
            $table->dropColumn('mode_info');
        });
    }
}
